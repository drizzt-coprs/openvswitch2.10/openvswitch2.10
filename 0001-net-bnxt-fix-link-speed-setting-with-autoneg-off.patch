From 3bdf79031ac9991210af55902aa632adde32d938 Mon Sep 17 00:00:00 2001
From: Ajit Khaparde <ajit.khaparde@broadcom.com>
Date: Sat, 24 Feb 2018 21:02:16 -0800
Subject: [PATCH] net/bnxt: fix link speed setting with autoneg off

[ backported from upstream commit 90cc14d77359bb3f8e48f4ef966ee77a52703949 ]

When Autoneg is turned off especially on remote side,
link does not come up. This patch fixes that.

Fixes: 7bc8e9a227cc ("net/bnxt: support async link notification")

Signed-off-by: Ajit Khaparde <ajit.khaparde@broadcom.com>
---
 drivers/net/bnxt/bnxt.h      |  1 +
 drivers/net/bnxt/bnxt_hwrm.c | 25 ++++++++++++++++++++++---
 2 files changed, 23 insertions(+), 3 deletions(-)

diff --git a/dpdk-17.11/drivers/net/bnxt/bnxt.h b/dpdk-17.11/drivers/net/bnxt/bnxt.h
index 8ab1c7f85..3bc2b9379 100644
--- a/dpdk-17.11/drivers/net/bnxt/bnxt.h
+++ b/dpdk-17.11/drivers/net/bnxt/bnxt.h
@@ -162,6 +162,7 @@ struct bnxt_link_info {
 	uint16_t		link_speed;
 	uint16_t		support_speeds;
 	uint16_t		auto_link_speed;
+	uint16_t		force_link_speed;
 	uint16_t		auto_link_speed_mask;
 	uint32_t		preemphasis;
 	uint8_t			phy_type;
diff --git a/dpdk-17.11/drivers/net/bnxt/bnxt_hwrm.c b/dpdk-17.11/drivers/net/bnxt/bnxt_hwrm.c
index 51b0056cd..ce214d7cb 100644
--- a/dpdk-17.11/drivers/net/bnxt/bnxt_hwrm.c
+++ b/dpdk-17.11/drivers/net/bnxt/bnxt_hwrm.c
@@ -738,7 +738,8 @@ static int bnxt_hwrm_port_phy_cfg(struct bnxt *bp, struct bnxt_link_info *conf)
 				HWRM_PORT_PHY_CFG_INPUT_AUTO_MODE_ALL_SPEEDS;
 		}
 		/* AutoNeg - Advertise speeds specified. */
-		if (conf->auto_link_speed_mask) {
+		if (conf->auto_link_speed_mask &&
+		    !(conf->phy_flags & HWRM_PORT_PHY_CFG_INPUT_FLAGS_FORCE)) {
 			req.auto_mode =
 				HWRM_PORT_PHY_CFG_INPUT_AUTO_MODE_SPEED_MASK;
 			req.auto_link_speed_mask =
@@ -801,12 +802,22 @@ static int bnxt_hwrm_port_phy_qcfg(struct bnxt *bp,
 	link_info->support_speeds = rte_le_to_cpu_16(resp->support_speeds);
 	link_info->auto_link_speed = rte_le_to_cpu_16(resp->auto_link_speed);
 	link_info->preemphasis = rte_le_to_cpu_32(resp->preemphasis);
+	link_info->force_link_speed = rte_le_to_cpu_16(resp->force_link_speed);
 	link_info->phy_ver[0] = resp->phy_maj;
 	link_info->phy_ver[1] = resp->phy_min;
 	link_info->phy_ver[2] = resp->phy_bld;
 
 	HWRM_UNLOCK();
 
+	RTE_LOG(DEBUG, PMD, "Link Speed %d\n", link_info->link_speed);
+	RTE_LOG(DEBUG, PMD, "Auto Mode %d\n", link_info->auto_mode);
+	RTE_LOG(DEBUG, PMD, "Support Speeds %x\n", link_info->support_speeds);
+	RTE_LOG(DEBUG, PMD, "Auto Link Speed %x\n", link_info->auto_link_speed);
+	RTE_LOG(DEBUG, PMD, "Auto Link Speed Mask %x\n",
+		    link_info->auto_link_speed_mask);
+	RTE_LOG(DEBUG, PMD, "Forced Link Speed %x\n",
+		    link_info->force_link_speed);
+
 	return rc;
 }
 
@@ -2124,7 +2135,9 @@ int bnxt_set_hwrm_link_config(struct bnxt *bp, bool link_up)
 	autoneg = bnxt_check_eth_link_autoneg(dev_conf->link_speeds);
 	speed = bnxt_parse_eth_link_speed(dev_conf->link_speeds);
 	link_req.phy_flags = HWRM_PORT_PHY_CFG_INPUT_FLAGS_RESET_PHY;
-	if (autoneg == 1) {
+	/* Autoneg can be done only when the FW allows */
+	if (autoneg == 1 && !(bp->link_info.auto_link_speed ||
+				bp->link_info.force_link_speed)) {
 		link_req.phy_flags |=
 				HWRM_PORT_PHY_CFG_INPUT_FLAGS_RESTART_AUTONEG;
 		link_req.auto_link_speed_mask =
@@ -2142,7 +2155,13 @@ int bnxt_set_hwrm_link_config(struct bnxt *bp, bool link_up)
 		}
 
 		link_req.phy_flags |= HWRM_PORT_PHY_CFG_INPUT_FLAGS_FORCE;
-		link_req.link_speed = speed;
+		/* If user wants a particular speed try that first. */
+		if (speed)
+			link_req.link_speed = speed;
+		else if (bp->link_info.force_link_speed)
+			link_req.link_speed = bp->link_info.force_link_speed;
+		else
+			link_req.link_speed = bp->link_info.auto_link_speed;
 	}
 	link_req.duplex = bnxt_parse_eth_link_duplex(dev_conf->link_speeds);
 	link_req.auto_pause = bp->link_info.auto_pause;
-- 
2.14.3

