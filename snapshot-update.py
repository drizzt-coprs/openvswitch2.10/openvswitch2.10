#!/usr/bin/env python

import re
import sys
import requests
import datetime
import subprocess


class Commit():
    def __init__(self, user, repo, branch):
        self.user = user
        self.repo = repo
        self.branch = branch
        self.url = ("https://api.github.com/repos/{}/{}/commits/{}"
                    "".format(user, repo, branch))

    def get(self):
        r = requests.get(self.url)
        self.full_commit = r.json()
        self.sha = self.full_commit['sha']
        author = self.full_commit['commit']['author']
        self.date = datetime.datetime.strptime(author['date'],
                                               '%Y-%m-%dT%H:%M:%SZ')

    def download(self, tgz):
        source_url = ("https://github.com/{}/{}/archive/{}.tar.gz#/"
                      "{}-{}.tar.gz".format(self.user, self.repo, self.sha,
                                            self.user, self.sha[0:7]))

        with open(tgz, 'wb') as tarball:
            response = requests.get(source_url)
            for chunk in response.iter_content(chunk_size=128):
                tarball.write(chunk)


class Spec():
    def __init__(self):
        self.re_commit = re.compile('^%global commit0 [0-9A-Fa-f]{40}$')
        self.re_date = re.compile('^%global date [0-9]{8}$')
        self.re_changelog = re.compile('^%changelog$')

    def load(self, filename):
        with open(filename, 'r') as spec:
            self.specfile = spec.readlines()

    def get_sha(self):
        for line in self.specfile:
            if self.re_commit.match(line):
                (rpmmacro, macroname, sha) = line.split()
                return sha
        return None

    def set_sha(self, sha):
        temp = []
        for line in self.specfile:
            if self.re_commit.match(line):
                temp.append("%global commit0 {}\n".format(sha))
            else:
                temp.append(line)
        self.specfile = temp

    def set_date(self, date):
        date_str = date.strftime('%Y%m%d')
        temp = []
        for line in self.specfile:
            if self.re_date.match(line):
                temp.append("%global date {}\n".format(date_str))
            else:
                temp.append(line)
        self.specfile = temp

    def add_changelog(self, sha):
        now = datetime.datetime.now()
        now_str = now.strftime('%a %b %d %Y')
        temp = []
        for line in self.specfile:
            if self.re_changelog.match(line):
                temp.append(line)
                temp.append("* {} Open vSwitch Bot <null@redhat.com> "
                            "- 2.10-0\n".format(now_str))
                temp.append("- Snapshot of branch-2.10 {}\n".format(
                              sha[0:12]))
                temp.append("\n")
            else:
                temp.append(line)
        self.specfile = temp

    def write(self, filename):
        with open(filename, 'r+') as spec:
            spec.seek(0)
            spec.writelines(self.specfile)


class Rhpkg():
    def __init__(self):
        pass

    def build(self, local=False):
        if local:
            ret = subprocess.run(['rhpkg', 'local'])
        else:
            ret = subprocess.run(['rhpkg', 'build'])
        if ret.returncode != 0:
            print("rhpkg build failed ret={}".format(ret.returncode))
            sys.exit(-1)

    def push_new_sources(self, newtgz):
        tarballs = [newtgz]
        with open('sources', 'r') as sources:
            for entry in sources.readlines():
                (sha, tarball) = entry.split()
                # FIXME: remove hardcoded name
                if tarball.find('openvswitch-') == 0:
                    continue
                tarballs.append(tarball)

        new_sources = ['rhpkg', 'new-sources'] + tarballs
        ret = subprocess.run(new_sources)
        if ret.returncode != 0:
            print("rhpkg new-sources failed, ret={}".format(ret.returncode))
            sys.exit(-1)

    def commit(self, message, specfile):
        ret = subprocess.run(['git', 'commit', '-m', message, specfile,
                              'sources', '.gitignore'])
        if ret.returncode != 0:
            print("git push failed, ret={}".format(ret.returncode))
            sys.exit(-1)

    def push(self):
        push = ['rhpkg', 'push']
        ret = subprocess.run(push)
        if ret.returncode != 0:
            print("rhpkg push failed, ret={}".format(ret.returncode))
            sys.exit(-1)


if __name__ == "__main__":
    specfile = "openvswitch2.10.spec"

    commit = Commit("openvswitch", "ovs", "branch-2.10")
    commit.get()
    tarball = "openvswitch-{}.tar.gz".format(commit.sha[0:7])
    commit.download(tarball)
    spec = Spec()
    spec.load(specfile)
    sha = spec.get_sha()
    if sha == commit.sha:
        print("Package is up-to-date")
        sys.exit(0)

    spec.set_date(commit.date)
    spec.set_sha(commit.sha)
    spec.add_changelog(commit.sha)
    spec.write(specfile)

    rhpkg = Rhpkg()
    rhpkg.build(local=True)
    rhpkg.push_new_sources(tarball)
    rhpkg.commit("Bot: Updated to {}".format(commit.sha), specfile)
    rhpkg.push()
    rhpkg.build()

print("Package updated to {}".format(commit.sha))
