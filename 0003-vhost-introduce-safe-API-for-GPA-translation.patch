From 1fd1b4807bdabc32953e2591e06ac9331edee76e Mon Sep 17 00:00:00 2001
From: Maxime Coquelin <maxime.coquelin@redhat.com>
Date: Mon, 23 Apr 2018 11:33:40 +0200
Subject: [PATCH 03/11] vhost: introduce safe API for GPA translation

This new rte_vhost_va_from_guest_pa API takes an extra len
parameter, used to specify the size of the range to be mapped.
Effective mapped range is returned via len parameter.

This issue has been assigned CVE-2018-1059.

Reported-by: Yongji Xie <xieyongji@baidu.com>
Signed-off-by: Maxime Coquelin <maxime.coquelin@redhat.com>
---
 lib/librte_vhost/rte_vhost.h           | 40 ++++++++++++++++++++++++++++++++++
 lib/librte_vhost/rte_vhost_version.map |  6 +++++
 lib/librte_vhost/vhost.h               |  2 +-
 3 files changed, 47 insertions(+), 1 deletion(-)

diff --git a/dpdk-17.11/lib/librte_vhost/rte_vhost.h b/dpdk-17.11/lib/librte_vhost/rte_vhost.h
index f653644..f2d6c95 100644
--- a/dpdk-17.11/lib/librte_vhost/rte_vhost.h
+++ b/dpdk-17.11/lib/librte_vhost/rte_vhost.h
@@ -143,4 +143,44 @@ struct vhost_device_ops {
 }
 
+/**
+ * Convert guest physical address to host virtual address safely
+ *
+ * This variant of rte_vhost_gpa_to_vva() takes care all the
+ * requested length is mapped and contiguous in process address
+ * space.
+ *
+ * @param mem
+ *  the guest memory regions
+ * @param gpa
+ *  the guest physical address for querying
+ * @param len
+ *  the size of the requested area to map, updated with actual size mapped
+ * @return
+ *  the host virtual address on success, 0 on failure
+ */
+static __rte_always_inline uint64_t
+rte_vhost_va_from_guest_pa(struct rte_vhost_memory *mem,
+						   uint64_t gpa, uint64_t *len)
+{
+	struct rte_vhost_mem_region *r;
+	uint32_t i;
+
+	for (i = 0; i < mem->nregions; i++) {
+		r = &mem->regions[i];
+		if (gpa >= r->guest_phys_addr &&
+		    gpa <  r->guest_phys_addr + r->size) {
+
+			if (unlikely(*len > r->guest_phys_addr + r->size - gpa))
+				*len = r->guest_phys_addr + r->size - gpa;
+
+			return gpa - r->guest_phys_addr +
+			       r->host_user_addr;
+		}
+	}
+	*len = 0;
+
+	return 0;
+}
+
 #define RTE_VHOST_NEED_LOG(features)	((features) & (1ULL << VHOST_F_LOG_ALL))
 
diff --git a/dpdk-17.11/lib/librte_vhost/rte_vhost_version.map b/dpdk-17.11/lib/librte_vhost/rte_vhost_version.map
index 1e70495..9cb1d8c 100644
--- a/dpdk-17.11/lib/librte_vhost/rte_vhost_version.map
+++ b/dpdk-17.11/lib/librte_vhost/rte_vhost_version.map
@@ -53,2 +53,8 @@ DPDK_17.08 {
 
 } DPDK_17.05;
+
+DPDK_17.11.2 {
+	global;
+
+	rte_vhost_va_from_guest_pa;
+} DPDK_17.08;
diff --git a/dpdk-17.11/lib/librte_vhost/vhost.h b/dpdk-17.11/lib/librte_vhost/vhost.h
index de300c1..16d6b89 100644
--- a/dpdk-17.11/lib/librte_vhost/vhost.h
+++ b/dpdk-17.11/lib/librte_vhost/vhost.h
@@ -391,5 +391,5 @@ uint64_t __vhost_iova_to_vva(struct virtio_net *dev, struct vhost_virtqueue *vq,
 {
 	if (!(dev->features & (1ULL << VIRTIO_F_IOMMU_PLATFORM)))
-		return rte_vhost_gpa_to_vva(dev->mem, iova);
+		return rte_vhost_va_from_guest_pa(dev->mem, iova, len);
 
 	return __vhost_iova_to_vva(dev, vq, iova, len, perm);
-- 
1.8.3.1

